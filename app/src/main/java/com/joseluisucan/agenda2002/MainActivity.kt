package com.joseluisucan.agenda2002

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.get
import androidx.navigation.findNavController
import com.joseluisucan.agenda2002.databinding.ActivityMainBinding
import com.joseluisucan.agenda2002.viewModel.MainViewModel

class MainActivity : AppCompatActivity() {
    lateinit var binding:ActivityMainBinding
    private val viewModel:MainViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

    }

    override fun onBackPressed() {

        when(findNavController(R.id.fragmentContainerView).currentDestination!!.id){
            R.id.personalListFragment->{

            }
            R.id.formularioFragment->{
                super.onBackPressed()
            }
        }

    }
}