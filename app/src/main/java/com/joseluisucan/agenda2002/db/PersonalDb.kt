package com.joseluisucan.agenda2002.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.joseluisucan.agenda2002.domain.dao.PersonalDao
import com.joseluisucan.agenda2002.model.Personal

@Database(
    entities = [Personal::class],
    version = 1
)
abstract class PersonalDb:RoomDatabase() {
    abstract fun personalDao():PersonalDao
}