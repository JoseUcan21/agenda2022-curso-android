package com.joseluisucan.agenda2002.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.joseluisucan.agenda2002.R
import com.joseluisucan.agenda2002.config.Constantes
import com.joseluisucan.agenda2002.model.Personal
import com.joseluisucan.agenda2002.ui.PersonalListFragmentDirections


class PersonalAdapter(private val dataSet: List<Personal>) :
    RecyclerView.Adapter<PersonalAdapter.ViewHolder>() {

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_list, viewGroup, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val p = dataSet[position]
        viewHolder.bindItem(p)
    }

    override fun getItemCount() = dataSet.size

    class ViewHolder(val view:View) : RecyclerView.ViewHolder(view) {
        val txtNombre:TextView
        val txtTelefono:TextView
        init {
            txtNombre = view.findViewById(R.id.txt_nombre)
            txtTelefono = view.findViewById(R.id.txt_telefono)
        }

        fun bindItem(p: Personal){
            txtNombre.text = p.nombre +" "+p.apellidos
            txtTelefono.text = p.telefono
            val action = PersonalListFragmentDirections.actionPersonalListFragmentToFormularioFragment(
                operacion = Constantes.OPERACION_UPDATE,
                idEditar = p.idEmpleado,
                persona = p
            )
            view.setOnClickListener {
                view.findNavController().navigate(action)
            }
        }
    }
}
