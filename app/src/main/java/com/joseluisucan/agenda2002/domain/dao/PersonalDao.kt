package com.joseluisucan.agenda2002.domain.dao

import androidx.room.*
import com.joseluisucan.agenda2002.model.Personal

@Dao
interface PersonalDao {
    @Query("SELECT * FROM Personal")
    suspend fun getAll():List<Personal>

    @Insert
    suspend fun insert(persona:Personal):Long

    @Update
    suspend fun update(persona:Personal):Int

    @Delete
    suspend fun delete(personal: Personal):Int

    @Query("SELECT * FROM Personal WHERE nombre LIKE '%' || :name || '%' OR apellidos LIKE '%' || :name || '%'")
    suspend fun getByName(name: String): List<Personal>
}