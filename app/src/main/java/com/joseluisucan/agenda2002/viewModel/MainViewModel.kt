package com.joseluisucan.agenda2002.viewModel

import android.util.Log
import androidx.databinding.BaseObservable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.joseluisucan.agenda2002.config.AgendaApplication.Companion.db
import com.joseluisucan.agenda2002.config.Constantes
import com.joseluisucan.agenda2002.model.Personal
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainViewModel:ViewModel() {
    private val _personalList = MutableLiveData<List<Personal>?>()
    val personalList : LiveData<List<Personal>?> get() = _personalList

    //-1 estado inicial, 0 -> error, 1->Exito
    private val _operacionExitosa = MutableLiveData<Int>()
    val operacionExitosa: LiveData<Int> get() = _operacionExitosa
    var operacion = Constantes.OPERACION_INSERT

    var idPersonal = MutableLiveData<Long>()
    val nombre = MutableLiveData<String>()
    val apellido = MutableLiveData<String>()
    val telefono = MutableLiveData<String>()
    val edad = MutableLiveData<Int>()
    val parametroBusqueda = MutableLiveData<String>()


    init {
        //edad.value = 18
    }

    fun iniciar(){
        _operacionExitosa.value = -1
        limpiarDatos()
        viewModelScope.launch {
            _personalList.value = withContext(Dispatchers.IO){
                db.personalDao().getAll()
            }
        }
    }

    fun guardarUsuario(){
        var mPersonal = Personal(0,nombre.value!!,apellido.value!!,telefono.value!!,edad.value!!)
        when(operacion){
            Constantes.OPERACION_INSERT->{
                viewModelScope.launch {
                    val result = withContext(Dispatchers.IO){
                        db.personalDao().insert(mPersonal)
                    }

                    if(result>0){
                        _operacionExitosa.value = 1
                    }
                    else{
                        _operacionExitosa.value = 0
                    }
                }
            }
            Constantes.OPERACION_UPDATE->{
                mPersonal.idEmpleado = idPersonal.value!!
                viewModelScope.launch {
                    val result = withContext(Dispatchers.IO){
                        db.personalDao().update(mPersonal)
                    }

                    if(result>0){
                        _operacionExitosa.value = 1
                    }
                    else{
                        _operacionExitosa.value = 0
                    }
                }
            }
        }
    }

    fun limpiarDatos(){
        nombre.value = ""
        apellido.value = ""
        telefono.value = ""
        edad.value = 18
    }

    fun borrarPersonal() {
        var mPersonal = Personal(idPersonal.value!!)
        viewModelScope.launch {
            val result = withContext(Dispatchers.IO){
                db.personalDao().delete(mPersonal)
            }
            if(result>0){
                _operacionExitosa.value = 1
            }
            else{
                _operacionExitosa.value = 0
            }
        }
    }

    fun buscarPersonal(){
        viewModelScope.launch {
            _personalList.value = withContext(Dispatchers.IO){
                if(parametroBusqueda.value?.trim().isNullOrEmpty()){
                    db.personalDao().getAll()
                }else{
                    db.personalDao().getByName(parametroBusqueda.value!!)
                }
            }
        }
    }
}