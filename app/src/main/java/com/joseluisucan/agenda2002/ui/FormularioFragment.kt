package com.joseluisucan.agenda2002.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.joseluisucan.agenda2002.R
import com.joseluisucan.agenda2002.config.Constantes
import com.joseluisucan.agenda2002.databinding.FragmentFormularioBinding
import com.joseluisucan.agenda2002.databinding.FragmentPersonalListBinding
import com.joseluisucan.agenda2002.ui.dialogs.DeleteDialogFragment
import com.joseluisucan.agenda2002.viewModel.MainViewModel



class FormularioFragment : Fragment(), DeleteDialogFragment.DeleteListener {
    private val model: MainViewModel by activityViewModels()
    private var _binding: FragmentFormularioBinding? = null
    private val binding get() = _binding!!
    val args: FormularioFragmentArgs by navArgs()
    lateinit var operacion:String
    lateinit var dialogoBorrar:DeleteDialogFragment
    var idEmpleado = 0L
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFormularioBinding.inflate(inflater,container,false)
        val view = binding.root

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        operacion = args.operacion
        idEmpleado = args.idEditar
        binding.modelo = model
        binding.lifecycleOwner = viewLifecycleOwner
        model.operacion = operacion
        dialogoBorrar = DeleteDialogFragment(this)

        model.operacionExitosa.observe(viewLifecycleOwner, Observer {
            if(it>0){
                Toast.makeText(requireContext(), "Éxito al guardar", Toast.LENGTH_SHORT).show()
                findNavController().navigate(R.id.action_formularioFragment_to_personalListFragment)
            }else if(it==0){
                Toast.makeText(requireContext(), "Ocurrió un error al guardar", Toast.LENGTH_SHORT).show()
            }
        })

        if(operacion == Constantes.OPERACION_UPDATE){
            binding.linearEditar.visibility = View.VISIBLE
            binding.btnGuardar.visibility = View.GONE
            val p = args.persona
            p?.let {
                model.idPersonal.value = p.idEmpleado
                model.nombre.value = p.nombre
                model.apellido.value = p.apellidos
                model.telefono.value = p.telefono
                model.edad.value = p.edad
            }
        }
        else if(operacion == Constantes.OPERACION_INSERT){
            binding.linearEditar.visibility = View.GONE
            binding.btnGuardar.visibility = View.VISIBLE
        }
        binding.btnBorrar.setOnClickListener {
            dialogoBorrar.show(requireActivity().supportFragmentManager,"")
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun borrarPersona() {
        model.borrarPersonal()
    }

}