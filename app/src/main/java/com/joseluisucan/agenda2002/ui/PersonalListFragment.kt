package com.joseluisucan.agenda2002.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.joseluisucan.agenda2002.adapters.PersonalAdapter
import com.joseluisucan.agenda2002.config.Constantes.OPERACION_INSERT
import com.joseluisucan.agenda2002.databinding.FragmentPersonalListBinding
import com.joseluisucan.agenda2002.viewModel.MainViewModel


class PersonalListFragment : Fragment() {
    private val model:MainViewModel by activityViewModels()
    private var _binding: FragmentPersonalListBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentPersonalListBinding.inflate(inflater,container,false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.modelo = model
        binding.lifecycleOwner = viewLifecycleOwner
        model.iniciar()
        binding.miRecycler.layoutManager = LinearLayoutManager(requireContext())
        model.personalList.observe(viewLifecycleOwner, Observer {
            binding.miRecycler.adapter = PersonalAdapter(it!!)
        })

        binding.btnNuevo.setOnClickListener {
            val action = PersonalListFragmentDirections.actionPersonalListFragmentToFormularioFragment(operacion = OPERACION_INSERT)
            findNavController().navigate(action)
        }

        model.parametroBusqueda.observe(viewLifecycleOwner, Observer {
            model.buscarPersonal()
        })


    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}