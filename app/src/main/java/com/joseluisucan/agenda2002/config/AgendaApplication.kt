package com.joseluisucan.agenda2002.config

import android.app.Application
import androidx.room.Room
import com.joseluisucan.agenda2002.db.PersonalDb

class AgendaApplication:Application() {
    companion object{
        lateinit var db:PersonalDb
    }

    override fun onCreate() {
        super.onCreate()
        db = Room.databaseBuilder(
            this,
            PersonalDb::class.java,
            "Personal"
        ).build()
    }
}