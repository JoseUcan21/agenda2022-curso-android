package com.joseluisucan.agenda2002.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
data class Personal(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name="id_empleado")     var idEmpleado:Long = 0,
    @ColumnInfo(name="nombre")          var nombre:String = "",
    @ColumnInfo(name="apellidos")       var apellidos:String = "",
    @ColumnInfo(name="telefono")        var telefono:String = "",
    @ColumnInfo(name="edad")            var edad:Int = 0
):Serializable